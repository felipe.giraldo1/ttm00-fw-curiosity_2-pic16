#include "../Inc/parser.h"

#define MAX_SIZE_COMMAND_VALUE 4
#define FIRST_VALUE_POSITION 2
#define COMMAND_POSITION 0

vu8_UART_Rx_command_struct vu8_UART_Rx_command_parsing;
uint8_t *u8_parsing_buffer;

/**
    * @brief   If the command has a valid instruction then is valid. If does not,
    *          is invalid.
    * @param    The received Rx command.
    * @retval   an uint8_t is returned if the command is valid or doesn't.
    */

uint8_t is_command_valid(vu8_UART_Rx_command_struct *command){
    if((command->u8_command == 'P') || (command->u8_command == 'C') || 
            (command->u8_command == 'W') || (command->u8_command == 'L')){
        return valid;
    }
    else
        return no_valid;
}
