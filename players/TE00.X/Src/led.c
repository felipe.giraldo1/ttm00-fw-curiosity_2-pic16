#include "../Inc/led.h"

led_status_struct LED_D4;

/**
    * @brief   Turn the led on
    * @param    Non
    * @retval   Non
    */

void LED4_ON(void){
    LATAbits.LATA5 = 1;
}

/**
    * @brief   Turn the led off.
    * @param    Non
    * @retval   Non
    */

void LED4_OFF(void){
    LATAbits.LATA5 = 0;
}

/**
    * @brief   Toggle the led.
    * @param    Non
    * @retval   Non
    */

void LED4_toggle(void){
    LATAbits.LATA5 ^= 1;
}

/**
    * @brief   Here the led parameters are set with the pin, ports and pwm values.
    *           Also, a copy is done only with the pwm value to be used in
    *           the interrupt. 
    * @param    Led struct with the pin and the port. Also an struct to get the
    *           pwm parameters.  
    * @retval   Non
    */
void LED_initialize(led_status_struct *led, 
        uint8_t pin, 
        uint8_t port){
    led->pin = pin;
    led->port = port;
    led->status = active;
}
