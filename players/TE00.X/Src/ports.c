#include "../Inc/ports.h"
#include <xc.h>

/**
    * @brief   Ports are cleaned, assign as input or output and analog or digital
    * @param    Non
    * @retval   Non
    */

void ports_initialize(void){
    // Ports
    LATA = CLEAR_PORTA;
    LATB = CLEAR_PORTB;
    LATC = CLEAR_PORTC;
    
    // Input or output 
    TRISA = ALL_PINS_IN_PORTA_OUTPUT;
    TRISB = RB5_INPUT;
    TRISC = RC0_AND_RC4_INPUT;
    
    // Analog or digital
    ANSELA = ALL_PINS_IN_PORTA_DIGITAL;
    ANSELB = ALL_PINS_IN_PORTB_DIGITAL; 
    ANSELC = RC0_ANALOG;
    
    PPSLOCK = UNLOCKED;
    RXPPS =  RB5_RX_ENABLE; 
    RB7PPS = RB7_TX_ENABLE; 
    RC5PPS = RC5_PPS_ENABLE_TO_PWM3;
    PPSLOCK = LOCKED;
}