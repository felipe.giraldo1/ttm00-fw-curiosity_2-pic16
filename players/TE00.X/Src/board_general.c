#include "../Inc/board_general.h"
#include <xc.h>
#include <string.h>
#include <stdint.h>
#include <stdint.h>

/**
    * @brief    peripherals start initialization 
    * @param    Non
    * @retval   Non
    */
void Board_initialize(void){
    // no PLL
    OSCCONbits.SPLLEN = 0; 
    // 16MHz
    OSCCONbits.IRCF = 0x0F; 
    // Clock defined by config bits
    OSCCONbits.SCS = 0x02;  
    
    ports_initialize();
    timer1_initialize();
    UART_initialize();
    LED_initialize(&LED_D4, BIT_5, PORT_A);
    
    //Enable Peripheral interrupts  
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 0x01;   
}

/**
    * @brief   The command is received and after that parsing process is done.
    * @param    Non
    * @retval   Non
    */
void Rx_handler(void){
    if (UART_is_unread()){
        UART_clear_unread();
        if(is_command_valid(&vu8_UART_Rx_command_parsing))
//            dfbgsdvas
        memset(vu8_UART_RX.buffer, 0, sizeof(vu8_UART_RX.buffer));       
    }
}


/**
  * @brief  This function goes in the main loop. Here is called all  UART Rx 
  *         process, the command is verified and set. After that the PWM process
  *         is done. 
  * @param    Non
  * @retval   Non
  */
void board_handler(void){
    Rx_handler();
}


/**
  * @brief  All interruptions happen here
  */
void __interrupt() IntCallback(void){
    //Timer 1 interrupt
    if(TMR1IF) 
    {
        TMR1IF = 0; 
        T1CONbits.TMR1ON = 0x00;
        TMR1H = 0xf0; 
        TMR1L = 0x5f; 
        T1CONbits.TMR1ON = 0x01;    
//        aca va la interrupcion
    }
    
    // UART TX interruption
    if (PIR1bits.TXIF) UART_TX_int();
   
    // UART RX interruption
    if (PIR1bits.RCIF) UART_RX_int();
}