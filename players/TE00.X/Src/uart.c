#include "../Inc/uart.h"
#include <xc.h>

vu8_UART_TX_structure vu8_UART_TX;
vu8_UART_RX_structure vu8_UART_RX;

void initialize_Tx_buffer_UART(vu8_UART_TX_structure *Tx_buffer_UART){
    Tx_buffer_UART->head = 0;
    Tx_buffer_UART->tail = 0;
}

void initialize_Rx_buffer_UART(vu8_UART_RX_structure *Rx_buffer_UART){
    Rx_buffer_UART->head = 0;
    Rx_buffer_UART->unread = 0;
}

/**
  * @brief  UART configuration
  */
void UART_initialize(void){
    GIE = DISABLE_BIT_INTERRUPT;     
    
    BAUD1CONbits.BRG16 = BAUD_RATE_16bits_GENERATOR; 
    
    SP1BRGH = 0x01; 
    SP1BRGL = 0xA0;
    
    TX1STAbits.SYNC = ASYNCHRONOUS_MODE; 
    TX1STAbits.BRGH = HIGH_SPEED; 
    TX1STAbits.TX9 = BITS_TRANSMISSION_8; 
    TX1STAbits.TXEN = TRANSMIT_ENABLED;
      
    RC1STAbits.SPEN = SERIAL_PORT_ENABLED; 
    RC1STAbits.CREN = ENABLE_RECEIVER_BIT;
    RC1STAbits.RX9 = BITS_RECEPTION_8;
    
    // Enable interrupts
    GIE = ENABLE_BIT_INTERRUPT; 
    initialize_Tx_buffer_UART(&vu8_UART_TX);
    initialize_Rx_buffer_UART(&vu8_UART_RX);
}

/**
  * @brief  This function is used in the interruption. Writes to the output UART Tx buffer
  */
void inline UART_TX_int(void)
{
    //There's data to be sent
    if(vu8_UART_TX.buffer_available < sizeof(vu8_UART_TX.buffer)){
        TX1REG = vu8_UART_TX.buffer[vu8_UART_TX.tail++];   
        
        //Overflow
        if(vu8_UART_TX.tail >= sizeof(vu8_UART_TX.buffer))      
        {
            vu8_UART_TX.tail = 0;
        }
        vu8_UART_TX.buffer_available++;                      
    }
    //No more data to send
    else                                                
    {
        //Disable interrupts
        PIE1bits.TXIE = DISABLE_ADC_INTERRUPT;                              
    }
}
/**
    * @brief    Writes a char via UART.
    * @param    1 byte to be sent.
    */
void UART_write(uint8_t TX_byte)
{
    // Waiting for the buffer
    while(vu8_UART_TX.buffer_available == 0){}    
    
    // First byte to send
    if(PIE1bits.TXIE == 0){
        TX1REG = TX_byte;             
    }
    // Data being sent / not done sending yet
    else {
        PIE1bits.TXIE = DISABLE_ADC_INTERRUPT;       
        // Buffer data
        vu8_UART_TX.buffer[vu8_UART_TX.head++] = TX_byte; 
        
        //Overflow
        if(vu8_UART_TX.head >= UART_TX_BUFFER_SIZE){
            vu8_UART_TX.head = 0;
        }
        vu8_UART_TX.buffer_available--; 
    }
    
    //Enable interrupt (there's data to be sent)
    PIE1bits.TXIE = ENABLE_ADC_INTERRUPT;              
}

/**
    * @brief    To be called when printf is used.
                This funtion replaces putch from stdio.h
    */
void putch(char Tx_data) {
    UART_write(Tx_data);
}

/**
    * @brief    Function checks if a message arrived
    */
void inline UART_RX_int(void){

    if(1 == RC1STAbits.OERR){
        // EUSART error - restart
        RC1STAbits.CREN = 0;
        RC1STAbits.CREN = 1;
    }
    
// buffer overruns are ignored
    vu8_UART_RX.buffer[vu8_UART_RX.head] = RC1REG;
    
    if(vu8_UART_RX.buffer[vu8_UART_RX.head] == '\r'){   
        vu8_UART_RX.unread = 1;
        vu8_UART_RX.head = 0;
    }
   
    else{
        vu8_UART_RX.head++;
    }

    if(vu8_UART_RX.head >= sizeof(vu8_UART_RX.buffer)){
        vu8_UART_RX.head = 0;
    }
}

/**
    * @brief    Returns the flag "unread" to know if there is a message waiting
    */
uint8_t UART_is_unread(void){
    return vu8_UART_RX.unread;
}

/**
    * @brief    Clean the "unread" flag 
    */
void UART_clear_unread(void){
    vu8_UART_RX.unread = 0;
}

/**
    * @brief    Clean the "unread" flag 
    */
uint8_t *get_Rx_buffer(void){
    return (uint8_t *) vu8_UART_RX.buffer;
}