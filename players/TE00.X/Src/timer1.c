#include "../Inc/timer1.h"
#include <xc.h>

/**
    * @brief   Configuration of timer 1 (here there is not an interruption yet)
    * @param    Non
    * @retval   Non
    */

void timer1_initialize(void){
    // TMR1 clock configuration
    T1CONbits.TMR1CS = FOSC_FOUR_MHz; 
    T1CONbits.T1CKPS = ONE_TO_ONE_PREESCALER; 
    T1CONbits.nT1SYNC = NO_SYNC_ASYNCHRONOUS_CLOCK; 
    T1GCONbits.TMR1GE = REGARDLESS_TIMER1_GATE_FUNCTION; 
    
    TMR1H = HIGH_VALUE_TO_SET_1ms_TICK;
    TMR1L = LOW_VALUE_TO_SET_1ms_TICK;
    
    //Clear TMR1 Flag and enable TMR1 and overflow interrupt
    PIR1bits.TMR1IF = CLEAR_INTERRUPT_FLAG; 
    PIE1bits.TMR1IE = ENABLE_TIMER1_OVERFLOW_INTERRUPT; 
    T1CONbits.TMR1ON = ENABLE_TIMER1; 
}