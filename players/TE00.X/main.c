/*
 * File:   main.c
 * Author: User26
 *
 * Created on July 14, 2020, 4:33 PM
 */


#include "Inc/main.h"
#include "Inc/board_general.h"

#define _XTAL_FREQ 16000000

#define HW_VER "1.1"
#define FW_VER "1.1"
#define AUTHOR "JFG"


void main(void) {
//    printf("Booting ...\n FW Version = %s, HW Version = %s.\n Author = %s.\n//",
//        FW_VER,HW_VER,AUTHOR );    
    Board_initialize();    
    while(1){
        board_handler();
    }
}
