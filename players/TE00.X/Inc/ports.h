#ifndef _PORTS_
#define _PORTS_

#include <xc.h>
#include "uart.h"

#define CLEAR_PORTA 0x00;
#define CLEAR_PORTB 0x00;
#define CLEAR_PORTC 0x00;

#define ALL_PINS_IN_PORTA_OUTPUT 0x00;
#define RB5_INPUT 0x20;
#define RC0_AND_RC4_INPUT 0x11;

#define ALL_PINS_IN_PORTA_DIGITAL 0x00;
#define ALL_PINS_IN_PORTB_DIGITAL 0x00;
#define RC0_ANALOG 0x01;

#define RB5_RX_ENABLE 0b00001101;
#define RB7_TX_ENABLE 0b00010010;
#define RC5_PPS_ENABLE_TO_PWM3 0b00001110;

#define UNLOCKED 0x00; 
#define LOCKED 0x01;

enum{
    PORT_A,
    PORT_B,
    PORT_C
};

enum{
    BIT_0,
    BIT_1,
    BIT_2,
    BIT_3,
    BIT_4,
    BIT_5,
    BIT_6,
    BIT_7
};

void ports_initialize(void);
#endif