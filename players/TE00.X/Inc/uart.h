#ifndef _UART_PIC16_
#define _UART_PIC16_

#include "stdio.h"
#include "stdint.h"

#define UART_TX_BUFFER_SIZE 32
#define ENABLE_BIT_INTERRUPT 0x01;
#define DISABLE_BIT_INTERRUPT 0x00;
#define ENABLE_ADC_INTERRUPT 0x01;
#define DISABLE_ADC_INTERRUPT 0x00;
#define BAUD_RATE_16bits_GENERATOR 0x01;
#define ASYNCHRONOUS_MODE 0x00;
#define HIGH_SPEED 0x01;
#define BITS_TRANSMISSION_8 0x00;
#define TRANSMIT_ENABLED 0x01;
#define SERIAL_PORT_ENABLED 0x01;
#define ENABLE_RECEIVER_BIT 0x01;
#define BITS_RECEPTION_8 0x00;

typedef struct{
    volatile uint8_t head;
    volatile uint8_t tail;
    volatile uint8_t buffer[UART_TX_BUFFER_SIZE];
    volatile uint8_t buffer_available;
}vu8_UART_TX_structure;

typedef struct{
    volatile uint8_t head;
    volatile uint8_t buffer[UART_TX_BUFFER_SIZE];
    volatile uint8_t unread;
}vu8_UART_RX_structure;

extern vu8_UART_RX_structure vu8_UART_RX;

void UART_initialize(void);
void inline UART_TX_int(void);
void UART_write(uint8_t TX_byte);

void inline UART_RX_int(void);
uint8_t UART_is_unread(void);
void UART_clear_unread(void);
uint8_t *get_Rx_buffer(void);
#endif 