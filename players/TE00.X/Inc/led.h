#ifndef _LED_
#define _LED_

#include <xc.h>
#include "stdint.h"
#include "stdio.h"

enum{
    inactive,
    active
};

typedef struct{
    uint8_t pin;
    uint8_t port;
    uint8_t status;
}led_status_struct;

extern led_status_struct LED_D4;

void LED4_ON(void);
void LED4_OFF(void);
void LED4_toggle(void);
void LED_initialize(led_status_struct *led, uint8_t pin, uint8_t port);
#endif