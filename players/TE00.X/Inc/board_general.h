#ifndef _BOARD_GENERAL_
#define _BOARD_GENERAL_

#include "main.h"
#include "ports.h"
#include "uart.h"
#include "timer1.h"
#include "led.h"
#include "parser.h"

void Board_initialize(void);
void Rx_handler(void);
void board_handler(void);
#endif