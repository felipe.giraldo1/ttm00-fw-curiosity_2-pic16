#ifndef _PARSER_
#define _PARSER_

#include "uart.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

enum{
    no_valid,
    valid
};

typedef struct{
    uint8_t u8_command;
    uint16_t u16_value;
}vu8_UART_Rx_command_struct;

extern vu8_UART_Rx_command_struct vu8_UART_Rx_command_parsing;

uint8_t is_command_valid(vu8_UART_Rx_command_struct *command);
#endif